module.exports = {
    getAnError: async function (promise) {
        let getAnError = false;
        try {
            await promise;
        }

        catch(error) {
            getAnError = true;
        }

        finally {
            return getAnError;
        }
    }
};